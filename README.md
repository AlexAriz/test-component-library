# Test Component Library
A test library to experiment creating a library of React components

## Features
* Uses Rollup to bundle the components
* Scss for styling components, enforcing CSS modules
* Tests using Jest
* Bundled Storybook for development

## Scripts
* `build` - Generate a new bundle
* `storybook` - Launch Storybook for development
* `test` - Run tests once (use in CI/CD pipeline)
* `test:watch` - Run tests in development, will re-run tests when changes are saved