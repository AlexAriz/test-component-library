import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames/bind'

import styles from './TestComponent.scss'
const cx = classnames.bind(styles)

const TestComponent = ({ theme }) =>
  <div
    data-testid='test-component'
    className={cx('test-component', { [`test-component-${theme}`]: !!theme })}
  >
    <h1 className={cx('heading')}>I'm the test component</h1>
    <h2>Hello World</h2>
  </div>

TestComponent.propTypes = {
  theme: PropTypes.oneOf(['secondary']),
}

export default TestComponent
