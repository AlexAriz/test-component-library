import React from 'react'
import { render } from '@testing-library/react'

import TestComponent from './TestComponent'
import styles from './TestComponent.scss'

describe('Test Component', () => {
  let props

  beforeEach(() => {
    props = {
      theme: 'secondary',
    }
  })

  const renderComponent = () => render(<TestComponent {...props} />)

  it('should have secondary className with theme set as secondary', () => {
    const { getByTestId } = renderComponent()

    const testComponent = getByTestId('test-component')

    expect(testComponent).toHaveClass(styles['test-component-secondary'])
  })
})
