import React from 'react'
import TestComponent from './TestComponent'

export default {
  title: 'TestComponent',
}

export const Primary = () => <TestComponent/>

export const Secondary = () => <TestComponent theme='secondary'/>
